<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2013 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

function editlinks_onDeleteAddon()
{
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';

    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('editlinks');
    } else {
        bab_removeEventListener('bab_eventBeforePageCreated' ,'editlinks_onBeforePageCreated', 'addons/editlinks/init.php');
    }
    return true;
}

function editlinks_upgrade($sVersionBase, $sVersionIni)
{

    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/addonsincl.php';
    include_once $GLOBALS['babInstallPath'].'admin/acl.php';

    $addon = bab_getAddonInfosInstance('editlinks');
    if (function_exists('bab_removeAddonEventListeners')) {
        bab_removeAddonEventListeners('editlinks');
        $addon->addEventListener('bab_eventBeforePageCreated', 'editlinks_onBeforePageCreated', 'init.php');
    } else {
        bab_addEventListener('bab_eventBeforePageCreated' ,'editlinks_onBeforePageCreated', 'addons/editlinks/init.php', 'editlinks');
    }


    if (empty($sVersionBase) || '0.0' === $sVersionBase)
    {
        // first install, set access rights to registered users

        if ($id_object = bab_addonsInfos::getAddonIdByName('editlinks', false))
        {
            aclSetGroups_registered('bab_addons_groups', $id_object);
        }
    }


    return true;
}


function editlinks_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    
	$babBody = bab_getBody();
    $addon = bab_getAddonInfosInstance('editlinks');

    if (!$addon->isAccessValid()) {
        return;
    }

    if ($jquery = bab_functionality::get('jquery')) {
        $jquery->includeCore();
    }

    $babBody->addJavascriptFile($addon->getTemplatePath().'editlinks.js', true);
    $babBody->addStyleSheet($addon->getStylePath().'editlinks.css');
    
    $Icons = bab_functionality::get('Icons');
    /*@var $Icons Func_Icons */
    $Icons->includeCss();
    
    $W = bab_Widgets();
    $metaWidget = $W->Label('', 'editlinksSelectors');
    $funcs = bab_functionality::getFunctionalities('ContextActions');

    foreach ($funcs as $name) {
    	$contextActions = bab_functionality::get('ContextActions/'.$name);
    	/* @var $contextActions Func_ContextActions */
    	$metaWidget->setMetadata($name, $contextActions->getClassSelector());
    }
    
    $babBody->babecho($metaWidget->display($W->HtmlCanvas()));
}